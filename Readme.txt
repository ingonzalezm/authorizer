#Authorizer
#Requirements
	Docker container
	Java
	
#This file contains:
	The resouce code in src/
	The pom.xml file to build the jar
	The DockerFile to build the image

#Package the authorizer API
	mvn clean package

# creak the image 
	docker build -t authorizer:0.0.1-SNAPSHOT .

#run the image 
	docker run -d -p 8081:8081 authorizer:0.0.1-SNAPSHOT


#Use Api Documentation: http://localhost:8081/swagger-ui/index.html

#Create an account 
	POST: http://localhost:8081/authorizer/create
	
	{ 
		"account": {
			"activeCard": true,
			"availableLimit":100 
		}
	}
	
#Creaate a transaction
	POST: http://localhost:8081/authorizer/transaction
	{ 
		"transaction": { 
			"merchant": "Burger King",
			"amount": 20,
			"time": //Optional "YYYY-MM-DD"
		}
	}
	
#More details: http://localhost:8081/swagger-ui/index.html