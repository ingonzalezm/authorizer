package com.nubank.authorizer.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Violation")
public class ViolationDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -645890765558584487L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "VIOLATION_ID")
	private Long violationId;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "ACCOUNT_ID")
	private AccountDto account;
	
}
