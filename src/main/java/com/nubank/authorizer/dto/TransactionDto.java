package com.nubank.authorizer.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Transaction")
public class TransactionDto implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1017452905240464719L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TRANSACTION_ID")
	private Long transactionId;
	
	@Column(name = "MERCHANT")
	private String merchant;
	
	@Column(name = "AMOUNT")
	private Integer amount;
	
	@Column(name = "TIME")
	private Date time;
	
	@ManyToOne
	@JoinColumn(name = "ACCOUNT_ID")
	private AccountDto account;
	
}
