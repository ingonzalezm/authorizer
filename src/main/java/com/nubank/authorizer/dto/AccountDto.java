package com.nubank.authorizer.dto;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Account")
public class AccountDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3741609444193628472L;
	
	@Id
	@Column(name = "ACCOUNT_ID")
	private Long accountId;
	
	@Column(name = "ACTIVE_CARD")
	private Boolean activeCard;
	
	@Column(name = "AVAILABLE_LIMIT")
	private Integer availableLimit;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "ACCOUNT_ID")
	private Collection<ViolationDto> violations;
	
	@OneToMany
	@JoinColumn(name = "ACCOUNT_ID")
	private Collection<TransactionDto> transactions;
	
}
