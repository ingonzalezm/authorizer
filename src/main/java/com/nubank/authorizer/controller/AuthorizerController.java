package com.nubank.authorizer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.AccountOperation;
import com.nubank.authorizer.model.TransactionOperation;
import com.nubank.authorizer.service.AuthorizeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/authorizer")
public class AuthorizerController {

	@Autowired
	AuthorizeService authorizeService;
	
	public AuthorizerController(AuthorizeService authorizeService) {
		this.authorizeService = authorizeService;
	}
	
	@GetMapping("/{accountId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get an account")
	public Account getByAccountId(@PathVariable Long accountId) {
		return authorizeService.getAccountBy(accountId);
	}
	
	@PostMapping(value="/create")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Creates a new account")
	public Account createAccount(@Valid @RequestBody AccountOperation accountOperation) {
		return authorizeService.createAccount(accountOperation.getAccount());
	}
	
	@PostMapping("/transaction")
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Creates a new transacction")
	public Account createTransaction(@Valid @RequestBody TransactionOperation transactionOperation) {
		return authorizeService.createTransaction(transactionOperation.getTransaction());
	}
	
}
