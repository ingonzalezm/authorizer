package com.nubank.authorizer.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import com.nubank.authorizer.dto.TransactionDto;

public interface TransactionRepository extends CrudRepository<TransactionDto, Long>{
	
	Collection<TransactionDto> findByTimeBetween(Date init, Date end);
}
