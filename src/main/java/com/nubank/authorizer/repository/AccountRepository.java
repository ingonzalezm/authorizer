package com.nubank.authorizer.repository;

import org.springframework.data.repository.CrudRepository;

import com.nubank.authorizer.dto.AccountDto;

public interface AccountRepository extends CrudRepository<AccountDto, Long>{

}
