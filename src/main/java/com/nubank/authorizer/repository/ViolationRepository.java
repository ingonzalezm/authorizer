package com.nubank.authorizer.repository;


import org.springframework.data.repository.CrudRepository;

import com.nubank.authorizer.dto.ViolationDto;

public interface ViolationRepository extends CrudRepository<ViolationDto, Long>{
	
}
