package com.nubank.authorizer.service;

import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.Transaction;

public interface AuthorizeService{
	
	/**
	 * Create an account
	 * @param account
	 * @return
	 */
	Account createAccount(Account account);

	/**
	 * Get account by accountId
	 * @param accountId
	 * @return
	 */
	Account getAccountBy(Long accountId);

	/**
	 * Create transaction
	 * @param transaction
	 * @return
	 */
	Account createTransaction(Transaction transaction);

}
