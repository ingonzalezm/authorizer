package com.nubank.authorizer.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nubank.authorizer.constants.Constants;
import com.nubank.authorizer.dto.AccountDto;
import com.nubank.authorizer.dto.TransactionDto;
import com.nubank.authorizer.dto.ViolationDto;
import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.Transaction;
import com.nubank.authorizer.repository.AccountRepository;
import com.nubank.authorizer.repository.TransactionRepository;
import com.nubank.authorizer.repository.ViolationRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AuthorizeServiceImpl implements AuthorizeService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	ViolationRepository violationRepository;

	ModelMapper modelMapper;

	private AuthorizeServiceImpl() {
		modelMapper = new ModelMapper();
	}

	@Override
	public Account createAccount(Account account) {
		Optional<AccountDto> optional = accountRepository.findById(1l);
		AccountDto accountDto = modelMapper.map(account, AccountDto.class);
		//If account does not exists
		if (!optional.isPresent()) {
			accountDto.setAccountId(1l);
			accountDto.setViolations(new ArrayList<>());
			accountRepository.save(accountDto);
		} else {
			saveViolation(optional.get(), Constants.ACCOUNT_ALREADY_INITIALIZED);
			return modelMapper.map(optional.get(), Account.class);
		}
		return modelMapper.map(accountDto, Account.class);
	}

	@Override
	public Account getAccountBy(Long accountId) {
		Optional<AccountDto> accountDto = accountRepository.findById(accountId);
		return modelMapper.map(accountDto.get(), Account.class);
	}

	@Override
	public Account createTransaction(Transaction transaction) {
		Optional<AccountDto> optional = accountRepository.findById(1l);
		if (optional.isPresent()) {
			AccountDto accountDto = optional.get();
			transaction.setTime(transaction.getTime()!=null?transaction.getTime():new Date());
			TransactionDto transactionDto = modelMapper.map(transaction, TransactionDto.class);
			transactionDto.setAccount(accountDto);
			if (validateTransaction(transactionDto, accountDto)) {
				accountDto.setAvailableLimit(accountDto.getAvailableLimit() - transactionDto.getAmount());
				accountRepository.save(accountDto);
				transactionRepository.save(transactionDto);
			}
			return modelMapper.map(accountDto, Account.class);
		}
		return null;

	}

	/**
	 * Validates transaction
	 * @param transactionDto
	 * @param accountDto
	 * @return
	 */
	private boolean validateTransaction(TransactionDto transactionDto, AccountDto accountDto) {

		Calendar dateEnd = Calendar.getInstance();
		dateEnd.setTime(transactionDto.getTime());

		Calendar dateInit = Calendar.getInstance();
		dateInit.setTimeInMillis(dateEnd.getTimeInMillis());
		dateInit.add(Calendar.MINUTE, -2);
		transactionDto.setTime(dateEnd.getTime());
		//Get transaction between now and 2 minutes before
		Collection<TransactionDto> transactions = transactionRepository.findByTimeBetween(dateInit.getTime(),
				dateEnd.getTime());
		
		//If there are less than 3 transactions
		if (transactions.size() < 3) {
			
			//Get transaction with the same merchant
			List<TransactionDto> sameTransactions = transactions.stream()
					.filter(t -> t.getMerchant().equalsIgnoreCase(transactionDto.getMerchant())
							&& t.getAmount().equals(transactionDto.getAmount()))
					.collect(Collectors.toList());
			
			//If there is no similar transactions
			if (sameTransactions.isEmpty()) {
				
				//If ammountLimit is greater than the amount transaction
				if (accountDto.getAvailableLimit() >= transactionDto.getAmount()) {
					return true;
				} else {
					log.info(Constants.ERROR_INSUFFICIENT_LIMIT);
					saveViolation(accountDto, Constants.ERROR_INSUFFICIENT_LIMIT);
				}
			} else {
				log.info(Constants.ERROR_DOUBLED_TRANSACTION);
				saveViolation(accountDto, Constants.ERROR_DOUBLED_TRANSACTION);
			}

		} else {
			log.info(Constants.ERROR_HIGH_FREQUENCY);
			saveViolation(accountDto, Constants.ERROR_HIGH_FREQUENCY);
		}
		return false;
	}

	/**
	 * Save Violation
	 * @param accountDto
	 * @param description
	 */
	private void saveViolation(AccountDto accountDto, String description) {
		log.info("Adding violation");
		ViolationDto violationDto = new ViolationDto();
		violationDto.setDescription(description);
		violationDto.setAccount(accountDto);
		violationRepository.save(violationDto);
	}

}
