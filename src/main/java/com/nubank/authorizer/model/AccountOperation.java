package com.nubank.authorizer.model;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountOperation {
	
	@Valid
	@NotNull
	@ApiModelProperty(value = "account", required = true)
	private Account account;
	
}
