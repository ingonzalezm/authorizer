package com.nubank.authorizer.model;


import lombok.Data;

@Data
public class Violation{
	
	private Long violationId;
	
	private String description;
	
}
