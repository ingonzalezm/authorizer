package com.nubank.authorizer.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Transaction {
	
	@ApiModelProperty(hidden = true)
	private Long transactionId;
	
	@NotNull
	@ApiModelProperty(example = "Walmart", value = "Merchant where the transaction is made", required = true)
	private String merchant;
	
	@NotNull
	@ApiModelProperty(example = "10", value = "Amount consumed", required = true, dataType = "number")
	private Integer amount;
	
	@ApiModelProperty(example = "2021-05-04", value = "Amount consumed", required = false)
	private Date time;

}
