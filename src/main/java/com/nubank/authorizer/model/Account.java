package com.nubank.authorizer.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Account {
	@ApiModelProperty(hidden = true)
	private Long accountId;
	
	@NotNull
	@ApiModelProperty(example = "true", value = "if the card is active set true, then false", required = true)
	private Boolean activeCard;
	
	@NotNull
	@ApiModelProperty(example = "100", value = "available amount limit", required = true, dataType = "number")
	private Integer availableLimit;
	
	@ApiModelProperty(hidden = true)
	private List<Violation> violations;
	
}
