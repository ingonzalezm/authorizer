package com.nubank.authorizer.constants;

public final class Constants {
	public static final String ERROR_DOUBLED_TRANSACTION = "doubled-transaction";
	public static final String ERROR_INSUFFICIENT_LIMIT = "insufficient-limit";
	public static final String ERROR_HIGH_FREQUENCY = "high-frequency-small-interval";
	public static final String ACCOUNT_ALREADY_INITIALIZED = "account-already-initialized";
	
	private Constants() {
		
	}
}
