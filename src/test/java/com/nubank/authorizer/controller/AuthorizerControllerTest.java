package com.nubank.authorizer.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.AccountOperation;
import com.nubank.authorizer.service.AuthorizeService;
import com.nubank.authorizer.utils.Utils;

@SpringBootTest
public class AuthorizerControllerTest {

	@Autowired
	AuthorizerController authorizerController;

	@MockBean
	AuthorizeService authorizeService;

	@Test
	public void testCreateAccount() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		when(authorizeService.createAccount(account)).thenReturn(Utils.getAccount());
		AccountOperation accountOperation = new AccountOperation();
		accountOperation.setAccount(account);
		Account createdAccount = authorizerController.createAccount(accountOperation);
		assertNotNull(createdAccount.getAccountId());
	}

	@Test
	public void testGetAccount() {
		when(authorizeService.getAccountBy(1l)).thenReturn(Utils.getAccount());
		Account createdAccount = authorizerController.getByAccountId(1l);
		assertNotNull(createdAccount);
	}

	@Test
	public void testExistingAccount() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		Account existingAccount = Utils.getAccount();
		existingAccount.setViolations(Utils.getViolations());
		when(authorizeService.createAccount(account)).thenReturn(existingAccount);
		AccountOperation accountOperation = new AccountOperation();
		accountOperation.setAccount(account);
		Account createdAccount = authorizerController.createAccount(accountOperation);
		assertTrue(createdAccount.getViolations().size() > 0);

	}

}
