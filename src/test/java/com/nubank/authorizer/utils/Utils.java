package com.nubank.authorizer.utils;

import java.util.ArrayList;
import java.util.List;

import com.nubank.authorizer.constants.Constants;
import com.nubank.authorizer.dto.AccountDto;
import com.nubank.authorizer.dto.TransactionDto;
import com.nubank.authorizer.dto.ViolationDto;
import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.Transaction;
import com.nubank.authorizer.model.Violation;

public class Utils {

	public static Account getAccount() {
		Account account = new Account();
		account.setAccountId(1l);
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		return account;
	}

	public static List<Violation> getViolations() {
		List<Violation> violations = new ArrayList<>();
		Violation violation = new Violation();
		violation.setDescription(Constants.ACCOUNT_ALREADY_INITIALIZED);
		violations.add(violation);
		return violations;
	}

	public static Transaction geTransaction() {
		Transaction transaction = new Transaction();
		transaction.setMerchant("Liverpool");
		transaction.setAmount(10);
		return transaction;
	}

	public static AccountDto getAccountDto() {
		AccountDto account = new AccountDto();
		account.setAccountId(1l);
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		account.setViolations(new ArrayList<ViolationDto>());
		return account;
	}
	
	public static TransactionDto geTransactionDto() {
		TransactionDto transaction = new TransactionDto();
		transaction.setMerchant("Liverpool");
		transaction.setAmount(10);
		return transaction;
	}

}
