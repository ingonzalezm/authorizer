package com.nubank.authorizer.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.nubank.authorizer.dto.AccountDto;
import com.nubank.authorizer.dto.TransactionDto;
import com.nubank.authorizer.dto.ViolationDto;
import com.nubank.authorizer.model.Account;
import com.nubank.authorizer.model.Transaction;
import com.nubank.authorizer.repository.AccountRepository;
import com.nubank.authorizer.repository.TransactionRepository;
import com.nubank.authorizer.repository.ViolationRepository;
import com.nubank.authorizer.utils.Utils;
@SpringBootTest
public class AuthorizerServiceTest {

	@Autowired
	AuthorizeService authorizeService;

	@MockBean
	AccountRepository accountRepository;

	@MockBean
	ViolationRepository violationRepository;

	@MockBean
	TransactionRepository transactionRepository;

	@Test
	public void testCreateAccount() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		when(accountRepository.findById(1L)).thenReturn(Optional.empty());
		when(violationRepository.save(new ViolationDto())).thenReturn(new ViolationDto());
		Account createdAccount = authorizeService.createAccount(account);
		assertNotNull(createdAccount.getAccountId());
		assertTrue(createdAccount.getViolations().isEmpty());
	}

	@Test
	public void testExistingAccount() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		Optional<AccountDto> optional = Optional.of(Utils.getAccountDto());
		when(accountRepository.findById(1L)).thenReturn(optional);
		when(violationRepository.save(any(ViolationDto.class))).thenReturn(any(ViolationDto.class));
		Account createdAccount = authorizeService.createAccount(account);
		assertFalse(createdAccount.getAccountId()!=1);
	}

	@Test
	public void testGetAccount() {
		Optional<AccountDto> optional = Optional.of(Utils.getAccountDto());
		when(accountRepository.findById(1L)).thenReturn(optional);
		Account account = authorizeService.getAccountBy(1l);
		assertNotNull(account);
	}

	@Test
	public void testCreateTrasaction() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		account.setViolations(new ArrayList<>());
		Optional<AccountDto> optional = Optional.of(Utils.getAccountDto());
		when(accountRepository.findById(1L)).thenReturn(optional);
		when(transactionRepository.save(any(TransactionDto.class))).thenReturn(any(TransactionDto.class));
		Account returnedAccount = authorizeService.createTransaction(Utils.geTransaction());
		assertNotNull(returnedAccount.getAvailableLimit() == 90);
	}
	
	@Test
	public void testDoubleTransaction() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		account.setViolations(new ArrayList<>());
		Optional<AccountDto> optional = Optional.of(Utils.getAccountDto());
		when(accountRepository.findById(1L)).thenReturn(optional);
		Account returnedAccount = authorizeService.createTransaction(Utils.geTransaction());
		assertNotNull(returnedAccount.getAvailableLimit() == 90);
		optional.get().setAvailableLimit(returnedAccount.getAvailableLimit());
		when(accountRepository.findById(1L)).thenReturn(optional);
		Collection<TransactionDto> transactions = new ArrayList<TransactionDto>();
		transactions.add(Utils.geTransactionDto());
		when(transactionRepository.findByTimeBetween(any(Date.class), any(Date.class))).thenReturn(transactions);
		authorizeService.createTransaction(Utils.geTransaction());
		assertNotNull(returnedAccount.getAvailableLimit() == 90);
	}
	
	@Test
	public void testExcededTrasaction() {
		Account account = new Account();
		account.setActiveCard(true);
		account.setAvailableLimit(100);
		account.setViolations(new ArrayList<>());
		Optional<AccountDto> optional = Optional.of(Utils.getAccountDto());
		when(accountRepository.findById(1L)).thenReturn(optional);
		when(transactionRepository.save(any(TransactionDto.class))).thenReturn(any(TransactionDto.class));
		Transaction transaction = new Transaction();
		transaction.setMerchant("Mercedes");
		transaction.setAmount(100000);
		Account returnedAccount = authorizeService.createTransaction(transaction);
		assertNotNull(returnedAccount.getAvailableLimit() == 100);
	}

}
